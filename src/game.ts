
import utils from "../node_modules/decentraland-ecs-utils/index"
import { ToggleState } from "../node_modules/decentraland-ecs-utils/toggle/toggleComponent"

/////////////////////////////////// SOME DEFINITIONS ////////////////////////////

// Define a black material
const blackMaterial = new Material()
blackMaterial.albedoColor = Color3.Black()
blackMaterial.roughness = 5

const blackMaterial2 = new Material()
blackMaterial2.albedoColor = Color3.Black()
blackMaterial2.metallic = 0.2

// Define a red material
const redMaterial = new Material()
redMaterial.albedoColor = Color3.Red()
redMaterial.metallic = 0.9


let openPos: Quaternion = Quaternion.Euler(0, 90, 0)
let closedPos: Quaternion = Quaternion.Euler(0, 0, 0)

// Define text entity on wall3

const textHolder = new Entity()
const myText = new TextShape("The middle path \nis the way on the top")
myText.fontSize = 1
myText.color = Color3.Blue()
textHolder.addComponent(new Transform({
  position: new Vector3(8, 8, 0.8),
  scale: new Vector3(10, 10, 0.01)
}))
textHolder.addComponent(myText)
engine.addEntity(textHolder)

// Define text entity on wall6

const textHolder2 = new Entity()
const myText2 = new TextShape("THIS GAME \nHAS NO NAME")
myText2.fontSize = 1
myText2.color = Color3.Blue()
textHolder2.addComponent(new Transform({
  position: new Vector3(8, 4, 15.01),
  rotation: Quaternion.Euler(0, 180, 0),
  scale: new Vector3(8, 8, 0.1)
}))
textHolder2.addComponent(myText2)
engine.addEntity(textHolder2)

//////////////////////////////////// WALLS////////////////////////////////////

const wall1 = new Entity()
wall1.addComponent(new Transform({
  position: new Vector3(11.75, 1, 1),
  scale: new Vector3(6.5, 2, 0.01)
}))
wall1.addComponent(new BoxShape())
wall1.addComponent(blackMaterial)
engine.addEntity(wall1)

const wall2 = new Entity()
wall2.addComponent(new Transform({
  position: new Vector3(4.25, 1, 1),
  scale: new Vector3(6.5, 2, 0.01)
}))
wall2.addComponent(new BoxShape())
wall2.addComponent(blackMaterial)
engine.addEntity(wall2)

const wall3 = new Entity()
wall3.addComponent(new Transform({
  position: new Vector3(8, 8, 1),
  scale: new Vector3(14, 12, 0.01)
}))
wall3.addComponent(new BoxShape())
wall3.addComponent(blackMaterial)
engine.addEntity(wall3)

const wall4 = new Entity()
wall4.addComponent(new Transform({
  position: new Vector3(1, 7, 8),
  rotation: Quaternion.Euler(0, 90, 0),
  scale: new Vector3(14.01, 14, 0.01)
}))
wall4.addComponent(new BoxShape())
wall4.addComponent(blackMaterial)
engine.addEntity(wall4)

const wall5 = new Entity()
wall5.addComponent(new Transform({
  position: new Vector3(8, 7, 15),
  scale: new Vector3(14, 14, 0.01)
}))
wall5.addComponent(new BoxShape())
wall5.addComponent(blackMaterial)
engine.addEntity(wall5)

const wall6 = new Entity()
wall6.addComponent(new Transform({
  position: new Vector3(15, 7, 8),
  rotation:  Quaternion.Euler(0, 90, 0),
  scale: new Vector3(14.01, 14, 0.01)
}))
wall6.addComponent(new BoxShape())
wall6.addComponent(blackMaterial)
engine.addEntity(wall6)

const floor = new Entity()
floor.addComponent(new Transform({
  position: new Vector3(8, 0, 8),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(16, 16, 0.01)
}))
floor.addComponent(new BoxShape())
floor.addComponent(blackMaterial2)
engine.addEntity(floor)

const ceiling = new Entity()
ceiling.addComponent(new Transform({
  position: new Vector3(8, 14, 8),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(14, 14, 0.01)
}))
ceiling.addComponent(new BoxShape())
ceiling.addComponent(blackMaterial)
engine.addEntity(ceiling)

//////////////////////////////////////// DOOR ///////////////////////////////////////////

// Add actual door to scene. This entity doesn't rotate, its parent drags it with it.
const door = new Entity()
door.addComponent(new Transform({
  position: new Vector3(0.5, 0, 0),
  scale: new Vector3(1, 2, 0.01)
}))
door.addComponent(new BoxShape())
engine.addEntity(door)
door.addComponent(blackMaterial)

// Define wrapper entity to rotate door. This is the entity that actually rotates.
const doorPivot = new Entity()
doorPivot.addComponent(new Transform({
  position: new Vector3(7.5, 1, 1),
  rotation: closedPos
}))
//doorPivot.addComponent(new DoorState())
engine.addEntity(doorPivot)

// Set the door as a child of doorPivot
door.setParent(doorPivot)

//toggle behavior for door
door.addComponent(new utils.ToggleComponent(utils.ToggleState.Off, value =>{
	if (value == utils.ToggleState.On){
		doorPivot.addComponentOrReplace(
			new utils.RotateTransformComponent(doorPivot.getComponent(Transform).rotation, openPos, 0.5)
			)
	}
	else{
		doorPivot.addComponentOrReplace(
			new utils.RotateTransformComponent(doorPivot.getComponent(Transform).rotation, closedPos, 0.5)
			)
	}
}))

// Set the click behavior for the door
door.addComponent(
  new OnClick(e => {
  door.getComponent(utils.ToggleComponent).toggle()
  source.playing = false
  sphere.removeComponent(blackMaterial)
  sphere.addComponent(redMaterial)
  })
)


/////////////////////////////////////////// STAIRS /////////////////////////////////////////////

const step2 = new Entity()
step2.addComponent(new Transform({
  position: new Vector3(15.5, 2, 3.5),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 1, 0.01)
}))
step2.addComponent(new BoxShape())
step2.addComponent(redMaterial)

const step3 = new Entity()
step3.addComponent(new Transform({
  position: new Vector3(15.5, 3, 5.5),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 1, 0.01)
}))
step3.addComponent(new BoxShape())
step3.addComponent(redMaterial)

const step4 = new Entity()
step4.addComponent(new Transform({
  position: new Vector3(15.5, 4, 7.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step4.addComponent(new BoxShape())
step4.addComponent(redMaterial)

const step5 = new Entity()
step5.addComponent(new Transform({
  position: new Vector3(15.5, 5, 9.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step5.addComponent(new BoxShape())
step5.addComponent(redMaterial)

const step6 = new Entity()
step6.addComponent(new Transform({
  position: new Vector3(15.5, 6, 11.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step6.addComponent(new BoxShape())
step6.addComponent(redMaterial)

const step7 = new Entity()
step7.addComponent(new Transform({
  position: new Vector3(15.5, 7, 13.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step7.addComponent(new BoxShape())
step7.addComponent(redMaterial)

const step8 = new Entity()
step8.addComponent(new Transform({
  position: new Vector3(15.5, 8, 15.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step8.addComponent(new BoxShape())
step8.addComponent(redMaterial)

const step9 = new Entity()
step9.addComponent(new Transform({
  position: new Vector3(13.5, 9, 15.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step9.addComponent(new BoxShape())
step9.addComponent(redMaterial)

const step10 = new Entity()
step10.addComponent(new Transform({
  position: new Vector3(11.5, 10, 15.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step10.addComponent(new BoxShape())
step10.addComponent(redMaterial)

const step11 = new Entity()
step11.addComponent(new Transform({
  position: new Vector3(9.5, 11, 15.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step11.addComponent(new BoxShape())
step11.addComponent(redMaterial)

const step12 = new Entity()
step12.addComponent(new Transform({
  position: new Vector3(7.5, 12, 15.5),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 1, 0.01)
}))
step12.addComponent(new BoxShape())
step12.addComponent(redMaterial)

const step13 = new Entity()
step13.addComponent(new Transform({
  position: new Vector3(5.5, 13, 15.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step13.addComponent(new BoxShape())
step13.addComponent(redMaterial)

const step14 = new Entity()
step14.addComponent(new Transform({
  position: new Vector3(3.5, 14, 15.5),
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(1, 1, 0.01)
}))
step14.addComponent(new BoxShape())
step14.addComponent(redMaterial)


//////////////////////////////////////////// SPHERE ///////////////////////////////////

// Add sphere inside the box
const sphere = new Entity()
sphere.addComponent(new Transform({
  position: new Vector3(8, 7, 8)}))
sphere.addComponent(new SphereShape())
sphere.addComponent(redMaterial)
engine.addEntity(sphere)

// Create AudioClip object, holding audio file
const clip = new AudioClip('sounds/laugh.mp3')
const source = new AudioSource(clip)
sphere.addComponent(source)
source.playing = false

sphere.addComponent(
	new OnClick(() => {
    source.playing = true
    source.loop = true

    sphere.removeComponent(redMaterial)
    sphere.addComponent(blackMaterial)

    if (door.getComponent(utils.ToggleComponent).isOn()){
      door.getComponent(utils.ToggleComponent).toggle()
    }

    engine.addEntity(step1)
    engine.addEntity(step2)
    engine.addEntity(step3)
    engine.addEntity(step4)
    engine.addEntity(step5)
    engine.addEntity(step6)
    engine.addEntity(step7)
    engine.addEntity(step8)
    engine.addEntity(step9)
    engine.addEntity(step10)
    engine.addEntity(step11)
    engine.addEntity(step12)
    engine.addEntity(step13)
    engine.addEntity(step14)
	})
)

//////////////////////////////////////////////STEP1/////////////////////////////////////

const step1 = new Entity()
step1.addComponent(new Transform({
  position: new Vector3(15.5, 1, 1.5),
  rotation: Quaternion.Euler(90, 0, 0), 
  scale: new Vector3(1, 1, 0.01)
}))
step1.addComponent(new BoxShape())
step1.addComponent(redMaterial)

/*
@Component("lerpData")
export class LerpData implements ISystem {
  origin: Vector3 = Vector3.Zero()
  target: Vector3 = Vector3.Zero()
  fraction: number = 0
}

// a system to carry out the movement
export class LerpMove {
  update(dt: number) {
    let transform = step1.getComponent(Transform)
    let lerp = step1.getComponent(LerpData)
    if (lerp.fraction < 1) {
      transform.position = Vector3.Lerp(
        lerp.origin,
        lerp.target,
        lerp.fraction
      )
      lerp.fraction += dt / 6
    }
  }
}

// Add system to engine
engine.addSystem(new LerpMove())

step1.addComponent(new LerpData())
step1.getComponent(LerpData).origin = new Vector3(15.5, 1, 1.5)
step1.getComponent(LerpData).target = new Vector3(15.5, 14, 1.5)
*/

const point1 = new Vector3(15.5, 1, 1.5)
const point2 = new Vector3(15.5, 14, 1.5)
const point3 = new Vector3(15.5, 1, 1.5)

const myPath = new Path3D([point1, point2, point3])

@Component("pathData")
export class PathData {
  origin: Vector3 = myPath.path[0]
  target: Vector3 = myPath.path[1]
  fraction: number = 0
  nextPathIndex: number = 1
}

export class PatrolPath implements ISystem {
  update(dt: number) {
    let transform = step1.getComponent(Transform)
    let path = step1.getComponent(PathData)
    if (path.fraction < 1) {
      transform.position = Vector3.Lerp(
        path.origin,
        path.target,
        path.fraction
      )
      path.fraction += dt / 6
    } else {
      path.nextPathIndex += 1
      if (path.nextPathIndex >= myPath.path.length) {
        path.nextPathIndex = 0
      }
      path.origin = path.target
      path.target = myPath.path[path.nextPathIndex]
      path.fraction = 0
    }
  }
}

engine.addSystem(new PatrolPath())

step1.addComponent(
	new OnClick(() => {
    step1.addComponent(new PathData())
  })
)
///////////////////////////////////////////////////// topSphere //////////////////////////////////////////

// Add sphere inside the box
const topSphere = new Entity()
topSphere.addComponent(new Transform({
  position: new Vector3(8, 15, 8)}))
topSphere.addComponent(new SphereShape())
topSphere.addComponent(redMaterial)
engine.addEntity(topSphere)

// Create AudioClip object, holding audio file
const clip2 = new AudioClip('sounds/finish.mp3')
const source2 = new AudioSource(clip2)
topSphere.addComponent(source2)
source2.playing = false

topSphere.addComponent(
	new OnClick(() => {
    source2.playOnce()

    if (door.getComponent(utils.ToggleComponent).isOn()){
      door.getComponent(utils.ToggleComponent).toggle()
    }

    engine.removeEntity(step1)
    engine.removeEntity(step2)
    engine.removeEntity(step3)
    engine.removeEntity(step4)
    engine.removeEntity(step5)
    engine.removeEntity(step6)
    engine.removeEntity(step7)
    engine.removeEntity(step8)
    engine.removeEntity(step9)
    engine.removeEntity(step10)
    engine.removeEntity(step11)
    engine.removeEntity(step12)
    engine.removeEntity(step13)
    engine.removeEntity(step14)
	})
)
