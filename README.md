## Open the door

Visitor should find the hidden door and click on it. It is right in the middle under the text "The middle path is the way on the top".

## Turn off the light

Visitor should click on the red sphere. It disappears and the door closes. Scary laughter ))

## Open the door again

Visitor should find the hidden door and click on it. Not that tricky, but still a bit scary.

## Climb on top

Visitor can find now a stairway on the top. Visitor should climb on it or...

## Take an elevator

Visitor can click on the first step when standing on it and it will elevate him on the top.

## Finish

Visitor should click on the red sphere. Robocop is glad. Everything starts from the beginning.

## Copyright info

This scene is protected with a standard Apache 2 licence. See the terms and conditions in the [LICENSE](/LICENSE) file.